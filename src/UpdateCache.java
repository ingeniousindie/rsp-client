import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.*;
import java.io.*;
import javax.swing.JOptionPane;

public class UpdateCache implements Runnable {

	private static final String CACHE_PATH = System.getProperty("user.home") + File.separator + "ValiusCacheV13" + File.separator; 
	private static final String ZIPPED_CACHE = CACHE_PATH + "cache.zip";
	private static final String DOWNLOAD_LINK = "https://dl.dropboxusercontent.com/s/bzgbjwgfa3g76us/ValiusCacheV13.zip"; 
	
	private Client client;
	
    public UpdateCache(Client client) { 
    	this.client = client;
    }
	
	@Override
	public void run() { 
		try {
			drawLoadingText(100, "Checking for updates...");
			System.setProperty("java.net.preferIPv4Stack", "true");
			System.setProperty("java.net.preferIPv6Addresses", "false");
			HttpURLConnection.setFollowRedirects(true);
			if (update(DOWNLOAD_LINK, ZIPPED_CACHE)) {
				unzip(CACHE_PATH);
				JOptionPane.showMessageDialog(null, "Update was completed successfully, please restart your client.", "Update Complete", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			}
		} catch (Throwable e) {
			JOptionPane.showMessageDialog(null, "Please notifiy an Administrator that you recieved this message.", "Update Failed", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Unzips the files to the desired path.
	 * @param cachePath
	 * 			the unzipped cache path.
	 */
	private void unzip(String cachePath) {
		byte[] buffer = new byte[1024];
		try {
			drawLoadingText(100, "Extracting...");
			File folder = new File(cachePath);
			if (!folder.exists())
				folder.mkdir();
			ZipInputStream in = new ZipInputStream(new FileInputStream(ZIPPED_CACHE));
			ZipEntry entry = in.getNextEntry();
			while (entry != null) {
				String fileName = entry.getName();
				File newFile = new File(cachePath + File.separator + fileName);
				new File(newFile.getParent()).mkdirs();
				FileOutputStream out = new FileOutputStream(newFile);
				int len;
				while ((len = in.read(buffer)) > 0)
					out.write(buffer, 0, len);
				out.close();
				entry = in.getNextEntry();
			}
			in.closeEntry();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Downloads a cache update if one is avaliable.
	 * @param url
	 * 			the cache url.
	 * @param filePath
	 * 			the destination path.
	 * @return
	 * 			if update was successful.
	 */
	private boolean update(String url, String filePath) {
		try {
			URLConnection connection = new URL(url).openConnection();
			connection.setUseCaches(false);
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);
			File file = new File(filePath);
			if (!file.exists())
				file.mkdirs();
			long contentLength = connection.getContentLengthLong();
			String checksum = null;
			if ((file.exists()) && (file.length() == contentLength) && ((checksum == null) || (checksum.equals(getMD5(file.toPath()))))) {
				return false;
			}
			drawLoadingText(100, "Downloading updates... Please wait a few moments..");
			File destination = new File(filePath);
			Files.copy(connection.getInputStream(), destination.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
			return true;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Gets an MD5 digest of the file contents.
	 * @param path
	 * 			path to file.
	 * @return	
	 * 			an MD5 message digest of the file contents.
	 */
	private String getMD5(Path path) throws NoSuchAlgorithmException, IOException {
		return new String(MessageDigest.getInstance("MD5").digest(Files.readAllBytes(path)));
	}
	 
	private void drawLoadingText(int amount, String text) {
		client.drawLoadingText(amount, text);
	}

}